# TCP/IP
## 计算机使用演变过程
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/6.png)
## 协议分层与OSI参考模型
协议分层就如同计算机软件中的模块化开发，OSI参考模型的建议是比较理想化的。
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/7.png)
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/8.png)
- 发送方从第7层、第6层到第1层由上至下按照顺序传输数据，而接收端则从第1层、第2层到第7层由下至上向每个上一级分层传输数据。每个分层上，在处理由上一层传过来的数据时可以附上当前分层的协议所必须的“首部”信息。然后接收端对收到的数据进行数据“首部”与“内容”的分离，再转发给上一分层，并最终将发送端的数据恢复为原装。
## IP的三大作用模块
- IP寻址

　　在计算机通信中，为了识别通信段，必须要有一个类似于地址的识别码进行标识。而在数据链路层，使用MAC地址来标识同一个链路中不同计算机的一种识别码。在网络层，则叫做IP地址。

- 路由（最终节点为止的转发）

　　路由控制（Routing）是指将分组数据发送到最终目标地址的功能。即使网络非常复杂，也可以通过路由控制确定到达目标地址的通路。因此，一个数据包之所以能够成功地到达最终的目标地址，全靠路由控制。

　　Hop中文叫“跳”，它是指网络中的一个区间，IP包正是在网络中一个跳间被转发。数据链路实现某一个区间（一跳）内的通信，而IP实现直至最终目标地址的通信（点对点）。

为了将数据包发送给目标主机，所有主机都维护者一张路由控制表（Routing Table），该表记录IP数据在下一步应该发给哪一个路由器。IP包将根据这个路由表在各个数据链路上传输。
- IP分包与组包

　　IP面向无连接，即在发包之前，不需要建立与对端目标地址之间的连接。上层如果遇到需要发送给IP的数据，该数据会被立即压缩成IP包发送出去。
## 　6.IP隧道
在一个网络环境中，假如网络A、B使用IPv6，中间位置的网络C支持使用IPv4的话，网络A与网络B之间无法直接进行通信。为了让他们之间正常通信，这时需要采用IP隧道的功能。
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/9.png)

IP隧道中可以将那些从网络A发过来的IPv6的包统和为一个数据，再为之追加一个IPv4的首部以后转发给网络C，这种在网络层的首部后面继续追加网络层首部的通信方法就叫做“IP隧道”。
![](![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/10.png)
## TCP
- TCP作为一种面向有连接的协议，只有在确认通信对端存在时才会发送数据，从而可以控制通信流量的浪费。

- 为了通过IP数据报实现可靠性传输，需要考虑很多事情，例如：数据的破坏、丢包、重复以及分片顺序混乱等问题。

- TCP通过检验和、序列号、确认应答、重发控制、连接管理以及窗口控制等机制实现可靠性传输。

- 使用TCP的一个连接的建立与断开，正常过程下至少需要来回发送7个包才能完成，也就是我们常常听到的三次握手，两次挥手。
## 网络的构成要素
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/12.png)
- 网卡：
计算机连接网络时,必须要使用网卡,也被称作网络适配器、LAN卡。
- 中继器
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/13.png)
- 网桥
位于OSI模型中的第二层--数据链路层上连接两个网络的设备。
## 现代网络实态
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/14.png)
![](https://gitee.com/qzbella/jekyll-resume/raw/master/images/15.png)